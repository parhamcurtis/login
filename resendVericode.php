<?php 
  include('head.php');
  $errors = [];
  $post = [];

  if($_POST) {
    $post = cleanPost($_POST);
    //validation
    $email = $post['email'];
    if(empty($email)) {
      $errors[] = "Email is required.";
    }

    if(empty($errors)) {
      $user = findUserByEmail($email);

      if(!$user) {
        $errors[] = "That user does not exist.";
      } else {
        $sent = send_vericode($email);
        if($sent) {
          redirect('login.php');
        }
      }
    }
  }

?>

<h2>Resend Verification Email</h2>
<p>You must verify your account before logging in. Please check your inbox and spam folders.</p>
<p>If you did not receive the verification email you may request a new verification email below.</p>

<?php displayErrors($errors); ?>

<form action="resendVericode.php" method="POST">
  <div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" id="email" 
      value="<?= issetElse($post, 'email', "");?>" placeholder="Email"
    />
  </div>

  <div class="button-wrapper">
    <a href="login.php" class="btn btn-secondary">Cancel</a>
    <input type="submit" class="btn btn-primary" value="Resend Verification Email" />
  </div>
</form>
<?php include('foot.php'); ?>