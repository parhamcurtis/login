<?php 
  include('head.php');
  if($_POST) {
    $post = cleanPost($_POST);
    dnd($post);
  }
?>
<form action="" method="POST">

  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" />
  </div>

<div class="form-group">
  <label>What are your hobbies?</label>
  <label for="hiking" class="checkbox">Hiking<input type="checkbox" id="hiking" name="hobbies[]" value="hiking" /></label>
  <label for="camping" class="checkbox">Camping<input type="checkbox" id="camping" name="hobbies[]" value="camping" /></label>
  <label for="video_games" class="checkbox">Video Games<input type="checkbox" id="video_games" name="hobbies[]" value="video_games" /></label>
  <label for="coding" class="checkbox">Coding<input type="checkbox" id="coding" name="hobbies[]" value="coding" /></label>
  <label for="board_games" class="checkbox">Board Games<input type="checkbox" id="board_games" name="hobbies[]" value="board_games" /></label>

</div>

<div class="button-wrapper">
  <input type="submit" value="Save" class="btn btn-primary" />
</div>
</form>


<?php include('foot.php');?>