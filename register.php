<?php 
  include('head.php');
  $errors = [];
  $post = [];

  if($_POST) {
    $post = cleanPost($_POST);
    
    // check for required fields
    $required = [
      'first_name' => "First Name",
      'last_name' => "Last Name",
      'email' => "Email",
      'password' => "Password",
      "confirm" => "Confirm Password"
    ];

    foreach($required as $field => $display) {
      if(empty($post[$field])) {
        $errors[] = "{$display} is required."; 
      }
    }

    // check passwords match
    if(empty($errors)) {
      if($post['password'] !== $post['confirm']) {
        $errors[] = "Passwords do not match.";
      }
    }

    // check valid email 
    if(empty($errors)) {
      if(!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
        $errors[] = "You must provide a valid email.";
      }
    }

    // check for duplicate email in db 
    if(empty($errors)) {
      $email = $post['email'];
      $row = findUserByEmail($email);
      if($row) {
        $errors[] = "That email address is already in use.";
      }
    }

    // if empty errors insert into db
    if(empty($errors)) {
      $fn = $post['first_name'];
      $ln = $post['last_name'];
      $email = $post['email'];
      $password = $post['password'];
      $password = password_hash($password, PASSWORD_DEFAULT);
      $sql = "INSERT INTO users (`first_name`, `last_name`, `email`, `password`) VALUES (?, ?, ?, ?);";
      $binds = [$fn, $ln, $email, $password];
      $result = query($sql, $binds, true);

      if($result) {
        $vericodeSent = send_vericode($email);
        if($vericodeSent) {
          // redirect to login.php 
          redirect('login.php');
        }
      }
    }
  }

?>
  <h2>Register</h2>

  <?php displayErrors($errors); ?>

  <form action="register.php" method="POST">
    <div class="form-group">
      <label for="first_name">First Name</label>
      <input type="text" value="<?= issetElse($post, 'first_name') ?>" name="first_name" id="first_name" placeholder="First Name">
    </div>

    <div class="form-group">
      <label for="last_name">Last Name</label>
      <input type="text" value="<?= issetElse($post, 'last_name') ?>" name="last_name" id="last_name" placeholder="Last Name">
    </div>

    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" value="<?= issetElse($post, 'email') ?>" name="email" id="email" placeholder="Email">
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" value="<?= issetElse($post, 'password') ?>" name="password" id="password" placeholder="Password">
    </div>

    <div class="form-group">
      <label for="confirm">Confirm Password</label>
      <input type="password" value="<?= issetElse($post, 'confirm') ?>" name="confirm" id="confirm" placeholder="Confirm Password">
    </div>

    <div class="button-wrapper">
      <button class="btn btn-primary">Register</button>
    </div>

    <p>Already have an account? <a href="login.php">Log In</a></p>
  </form>
<?php include('foot.php'); ?>