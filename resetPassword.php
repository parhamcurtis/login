<?php 
  include('head.php');
  $errors = [];
  $userId = issetElse($_SESSION, 'password_reset_user_id', 0);
  $code = issetElse($_SESSION, 'password_reset_code_verified', 0);
  $post = [];
  if($_POST) {
    $post = cleanPost($_POST);
    $password = $post['password'];
    $confirm = $post['confirm'];
    //verification
    $required = ['password' => "Password", 'confirm' => "Confirm Password"];
    foreach($required as $field => $display) {
      if(empty($post[$field])){
        $errors[] = "{$display} is required.";
      }
    }

    if($userId == 0 || $code == 0) {
      $errors[] = "Something has gone wrong. Please try again.";
    }

    if($password !== $confirm) {
      $errors[] = "Your passwords do not match.";
    }

    if(empty($errors)) {
      $hashed = password_hash($password, PASSWORD_DEFAULT);
      $sql = "UPDATE users SET password = ? WHERE id = ?";
      $binds = [$hashed, $userId];
      $result = query($sql, $binds, true);
      if($result) {
        unset($_SESSION['password_reset_user_Id'], $_SESSION['password_reset_code_verified']);
        $expired = date("Y-m-d H:i:s", strtotime("-14 days"));
        $sql = "DELETE FROM password_resets WHERE user_id = ? OR created_at < ?";
        $binds = [$userId, $expired];
        query($sql, $binds, true);
        redirect('login.php');
      } else {
        $errors[] = "Something has gone wrong. Please try again.";
      }
    }
  }
?>

<h2>Reset Password</h2>
<?php displayErrors($errors); ?>

<form action="resetPassword.php" method="POST">
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" placeholder="Password">
  </div>

  <div class="form-group">
    <label for="confirm">Confirm Password</label>
    <input type="password" name="confirm" id="confirm" placeholder="Confirm Password">
  </div>

  <div class="button-wrapper">
    <a href="login.php" class="btn btn-secondary">Cancel</a>
    <input class="btn btn-primary" type="submit" value="Reset Password" />
  </div>
</form>

<?php include('foot.php'); ?>