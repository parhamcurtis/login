<?php 
  include('head.php');
  $errors = [];
  $post = [];

  if($_POST) {
    $post = cleanPost($_POST);
    $email = $post['email'];
    //validation
    if(empty($email)) {
      $errors[] = "Email is required.";
    }

    if(empty($errors)) {
      $user = findUserByEmail($email);

      if(!$user) {
        $errors[] = "That user does not exist.";
      } else {
        //set up the password reset
        $code = rand(100001,999999);
        $email = $user['email'];
        $id = $user['id'];
        $time = date("Y-m-d H:i:s");
        $sql = "INSERT INTO password_resets (created_at, user_id, verify) VALUES
          (?,?,?);
        ";
        $binds = [$time, $id, $code];
        $result = query($sql, $binds, true);
        if($result) {
          //send an email with the code
          $msg = "<p>Your verification code to reset your password is</p>
            <p>{$code}</p>
            <p>This code will expire in 10 minutes.</p>
          ";

          send_mail($email, "Reset Your Password", $msg);
          $_SESSION['password_reset_user_id'] = $id;
          redirect('resetVerify.php');
        } 
      }
    }
  }
?>

<h2>Reset Your Password</h2>
<?php displayErrors($errors); ?>

<form action="" method="POST">

  <div class="form-group">
    <label for="email">Email</label>
    <input name="email" id="email" placeholder="Email" 
      value="<?= issetElse($post, 'email', "")?>" 
    />
  </div>

  <div class="button-wrapper">
    <a href="login.php" class="btn btn-secondary">Cancel</a>
    <input class="btn btn-primary" type="submit" value="Recover Password" />
  </div>
</form>

<?php include('foot.php');?>