<?php 
  include('head.php');
  $errors = [];
  $post = [];

  if($_POST) {
    $post = cleanPost($_POST);
    $code = $post['code'];
    $now = date("Y-m-d H:i:s");
    $userId = issetElse($_SESSION, 'password_reset_user_id',0);
    // validation
    if(empty($code)) {
      $errors[] = "Please enter your 6 digit code.";
    }

    if(empty($errors)) {
      $sql = "SELECT * FROM password_resets WHERE verify = ? AND user_id = ? ORDER BY id DESC LIMIT 1";
      $binds = [$code, $userId];
      $result = query($sql, $binds);
      $reset = mysqli_fetch_assoc($result);
      if(!$reset) {
        $errors[] = "The code you entered is incorrect or has expired.";
      } else {
        $expireTime = date("Y-m-d H:i:s", strtotime($reset['created_at'] . " +10 minutes"));
        $expired = strtotime($now) > strtotime($expireTime); 
  
        if($expired) {
          $errors[] = "Your code has expired. Please try again.";
        } else {
          $_SESSION['password_reset_code_verified'] = $code;
          redirect('resetPassword.php');
        }
      }

    }
  }
?>

<h2>Please Enter Your 6 digit code.</h2>
<p>A 6 digit code has been sent to your email address.</p>
<?php displayErrors($errors); ?>

<form action="resetVerify.php" method="POST">
  <div class="form-group">
    <label for="code">6 Digit Verification Code</label>
    <input type="text" name="code" id="code" placeholder="Verify" />
  </div>

  <div class="button-wrapper">
    <a href="login.php" class="btn btn-secondary">Cancel</a>
    <input class="btn btn-primary" type="submit" value="Verify" />
  </div>
</form>

<p>Resend Code? <a href="forgotPassword.php">Try Again</a></p>
<p><a href="login.php">Log In</a></p>



<?php include('foot.php');?>