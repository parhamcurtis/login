<?php 
  include('head.php');
  $current_user = current_user();

  if(!$current_user) {
    redirect('login.php');
  }

?>

<h2>Hello <?= $current_user['first_name'] ?></h2>
<a href="logout.php" class="btn btn-primary">Log Out</a>

<?php include('foot.php'); ?>