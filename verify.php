<?php
  include 'head.php';
  $vericode = sanitize($_GET['vericode']);
  $success = false;
  $msg = "Something has gone wrong or your account is already verified.";
  if($vericode) {
    $sql = "SELECT * FROM users WHERE verified = 0 AND vericode = ?";
    $binds = [$vericode];
    $result = query($sql, $binds);
    $user = mysqli_fetch_assoc($result);
    
    if($user) {
      // update verified column from 0 to 1 
      $sql = "UPDATE users SET verified = 1 WHERE id = ?";
      $binds = [$user['id']];
      $success = query($sql, $binds, true);
    }
  }

  if($success) {
    $msg = "Your account has been verified! Please log in.";
  }
?>

<h2><?= $msg ?></h2>
<div>
  <a href="login.php" class="btn btn-primary">Log In</a>
</div>

<?php include 'foot.php';?>