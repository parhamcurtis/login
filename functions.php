<?php 

  define('LOGIN_SESSION', 'ufhaks342lkjsf94');
  define('LOGIN_COOKIE', 'hwuf1743jfklsja2s');
  autologin();

  function dnd($data) {
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
    die;
  }

  function displayErrors($errors) {
    if(empty($errors)) return false;

    echo '<ul class="errors">';
    foreach($errors as $error) {
      echo "<li>{$error}</li>";
    }
    echo '</ul>';
  }

  function sanitize($dirty) {
    $clean = htmlentities($dirty, ENT_QUOTES, "UTF-8");
    return trim($clean);
  }

  function cleanPost($post) {
    $clean = [];
    foreach($post as $key => $value) {
      if(is_array($value)) {
        $ary = [];
        foreach($value as $val) {
          $ary[] = sanitize($val);
        }
        $clean[$key] = $ary;
      } else {
        $clean[$key] = sanitize($value);
      }
    }
    return $clean;
  }

  function issetElse($array, $key, $default = "") {
    if(!isset($array[$key]) || empty($array[$key])) {
      return $default;
    }
    return $array[$key];
  }

  function redirect($url) {
    if(!headers_sent()) {
      header("Location: {$url}");
    } else {
      echo '<script>window.location.href="' . $url . '"</script>';
    }
    exit;
  }

  function query($sql, $binds = [], $executeOnly = false) {
    $typeCast = "";
    foreach($binds as $bind) {
      $typeCast .= "s";
    }
    $db = mysqli_connect("127.0.0.1", "root", "", "login");
    $stmt = mysqli_stmt_init($db);
    mysqli_stmt_prepare($stmt, $sql);
    mysqli_stmt_bind_param($stmt, $typeCast, ...$binds);
    $result = mysqli_stmt_execute($stmt);
    if(!$executeOnly) {
      $result = mysqli_stmt_get_result($stmt);
    }
    mysqli_stmt_close($stmt);
    return $result;
  }

  function findUserByEmail($email) {
    $sql = "SELECT * FROM users WHERE email = ?";
    $binds = [$email];
    $result = query($sql, $binds);
    return mysqli_fetch_assoc($result);
  }

  function send_mail($to, $subject, $msg) {
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-Type:text/html;charset=UTF-8" . "\r\n";
    $headers .= "From: <curtis@freeskills.com>" . "\r\n";
    $result = mail($to, $subject, $msg, $headers);
    return $result;
  }

  function send_vericode($email) {
    $success = false;
    $user = findUserByEmail($email);

    if($user) {
      $vericode = md5(time());
      $sql = "UPDATE users SET vericode = ? WHERE id = ?";
      $result = query($sql, [$vericode, $user['id']], true);
      
      if($result) {
        $fn = $user['first_name'];
        $subject = "Please Verify Your Account";
        $msg = "<h3>{$fn},</h3><p>Thank you for regestering. Please verify your account by clicking 
          <a href=\"http://localhost/login/verify.php?vericode={$vericode}\" target=\"_blank\">here</a>.
        </p>";
        $success = send_mail($email, $subject, $msg);
      }
    }
    return $success;
  }

  function login($user, $remember = false) {
    $_SESSION[LOGIN_SESSION] = $user['id'];
    if($remember) {
      $hash = md5(time() . "_" . $user['id']);
      $fingerPrint = $_SERVER['HTTP_USER_AGENT'];
      $sql = "INSERT INTO sessions (user_id, hash, finger_print) VALUES (?,?,?)";
      $binds = [$user['id'], $hash, $fingerPrint];
      $result = query($sql, $binds, true);
      if($result) {
        $expiry = time() + (60 * 60 * 24 * 30);
        setcookie(LOGIN_COOKIE, $hash, $expiry);
      }
    }
  }

  function autologin() {
    if(isset($_COOKIE[LOGIN_COOKIE]) && !empty($_COOKIE[LOGIN_COOKIE])) {
      $cookie = $_COOKIE[LOGIN_COOKIE];
      $fingerPrint = $_SERVER['HTTP_USER_AGENT'];
      $sql = "SELECT * FROM sessions WHERE hash = ?";
      $binds = [$cookie];
      $result = query($sql, $binds);
      $session = mysqli_fetch_assoc($result);
      if($session && $session['finger_print'] == $fingerPrint) {
        $_SESSION[LOGIN_SESSION] = $session['user_id'];
      }
    }
  }

  function logout(){
    unset($_SESSION[LOGIN_SESSION]);
    if(isset($_COOKIE[LOGIN_COOKIE])) {
      $hash = $_COOKIE[LOGIN_COOKIE];
      $sql = "DELETE FROM sessions WHERE hash = ?";
      $binds = [$hash];
      query($sql, $binds, true);
      setcookie(LOGIN_COOKIE, '', time() - 1);
    }
  }

  function current_user() {
    $sessionExists = isset($_SESSION[LOGIN_SESSION]);
    if(!$sessionExists) {
      return false;
    }

    $id = $_SESSION[LOGIN_SESSION];
    $sql = "SELECT * FROM users WHERE id = ?";
    $binds = [$id];
    $result = query($sql, $binds);
    $user = mysqli_fetch_assoc($result);
    return $user? $user : false;
  }

?>