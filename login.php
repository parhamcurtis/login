<?php 
  include 'head.php';
  $post = [];
  $errors = [];

  if($_POST) {
    $post = cleanPost($_POST);
    $email = $post['email'];
    // form validation 
    $required = ['email' => 'Email', 'password' => 'Password'];
    foreach($required as $field => $display) {
      if(empty($post[$field])) {
        $errors[] = "{$display} is required.";
      }
    }

    if(empty($errors)){
      $user = findUserByEmail($email);
      $matches = false;
      
      if($user){
        if($user['verified'] != 1) {
          redirect('resendVericode.php');
        }

        // check password 
        $matches = password_verify($post['password'], $user['password']);
        
        if($matches) {
          // log in user
          $remember = isset($post['remember_me']) && $post['remember_me'] == 'on';
          login($user, $remember);
          redirect('index.php');
        }
      }

      if(!$user || !$matches) {
        $errors[] = "Somthing is wrong with your email or password.";
      }
    }
  }
?>


<h2>Log In</h2>

<?php displayErrors($errors); ?>

<form action="login.php" method="POST">
  <div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" id="email" value="<?= issetElse($post, 'email',"")?>" placeholder="Email" />
  </div>

  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" id="password" value="<?= issetElse($post, 'password',"")?>" placeholder="Password" />
  </div>

  <label>
    Stay Logged In 
    <input type="checkbox" value="on" name="remember_me" />
  </label>

  <div class="button-wrapper">
    <button class="btn btn-primary">Log In</button>
  </div>

  <p>Don't have an account? <a href="register.php">Register</a></p>
  <p>Forgot Your Password? <a href="forgotPassword.php">Reset Password</a></p>
</form>


<?php include 'foot.php';?>